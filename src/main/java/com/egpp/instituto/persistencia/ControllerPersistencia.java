package com.egpp.instituto.persistencia;

import java.util.ArrayList;
import java.util.List;

import com.egpp.instituto.models.Alumno;
import com.egpp.instituto.models.Carrera;
import com.egpp.instituto.models.Materia;

public class ControllerPersistencia {
	AlumnoJpaController aluJpa = new AlumnoJpaController();
	CarreraJpaController carrJpa = new CarreraJpaController();
	MateriaJpaController matJpa = new MateriaJpaController();
	
	// ALUMNO
	
	// insert
	public void crearAlumno(Alumno alu) {
		aluJpa.create(alu);
	}
	
	// update
	public void actualizarAlumno(Alumno alu) {
		try {
			aluJpa.edit(alu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// delete
	public void eliminarAlumno(int id) {
		try {
			aluJpa.destroy(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// select * from alumno where id = 
	
	public Alumno obtenerAlumno(int id) {
		return aluJpa.findAlumno(id);
	}	
	
	
	// select * from alumno 
	
	
	public ArrayList<Alumno> obtenerTodosAlumnos(){
		List<Alumno> lista = aluJpa.findAlumnoEntities();
		
		ArrayList<Alumno> listaVector = new ArrayList<Alumno>(lista);
		
		return listaVector;
	}
	

	// CARRERA
	public void crearCarrera(Carrera carr) {
		carrJpa.create(carr);
	}
	
	// update
	public void actualizarCarrera(Carrera carr) {
		try {
			carrJpa.edit(carr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// delete
	public void eliminarCarrera(int id) {
		try {
			carrJpa.destroy(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Carrera obtenerCarrera(int id) {
		return carrJpa.findCarrera(id);
	}	
	
	
	
	public ArrayList<Carrera> obtenerTodosCarreras(){
		List<Carrera> lista = carrJpa.findCarreraEntities();
		
		ArrayList<Carrera> listaVector = new ArrayList<Carrera>(lista);
		
		return listaVector;
	}
	
	
	// Materia
		public void crearMateria(Materia materia) {
			matJpa.create(materia);
		}
		
		// update
		public void actualizarMateria(Materia materia) {
			try {
				matJpa.edit(materia);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// delete
		public void eliminarMateria(int id) {
			try {
				matJpa.destroy(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public Materia obtenerMateria(int id) {
			return matJpa.findMateria(id);
		}	
		
		
	
		public ArrayList<Materia> obtenerTodosMaterias(){
			List<Materia> lista = matJpa.findMateriaEntities();
			
			ArrayList<Materia> listaVector = new ArrayList<Materia>(lista);
			
			return listaVector;
		}
	
}
