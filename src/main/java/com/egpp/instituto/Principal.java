package com.egpp.instituto;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import com.egpp.instituto.models.Alumno;
import com.egpp.instituto.models.Carrera;
import com.egpp.instituto.models.Materia;
import com.egpp.instituto.persistencia.AlumnoJpaController;
import com.egpp.instituto.persistencia.CarreraJpaController;
import com.egpp.instituto.persistencia.ControllerPersistencia;

public class Principal {

	public static void main(String[] args) {
		System.out.println("Introducción al JPA");

		ControllerPersistencia contPers = new ControllerPersistencia();

		// carrera
		Carrera carr = new Carrera(34, "Ingenieria de Sistemas", new LinkedList<Materia>());

		contPers.crearCarrera(carr);

		// generar materias
		Materia mat1 = new Materia(12, "Programacion 1", "Semestral", carr);
		Materia mat2 = new Materia(13, "Estructura de datos", "Semestral", carr);
		Materia mat3 = new Materia(15, "Taller de Programacion", "Semestral", carr);
		Materia mat4 = new Materia(16, "Taller de Tesis", "Anual", carr);

		contPers.crearMateria(mat1);
		contPers.crearMateria(mat2);
		contPers.crearMateria(mat3);
		contPers.crearMateria(mat4);

		// generamos las carrera

		LinkedList<Materia> listaMaterias = new LinkedList<Materia>();

		listaMaterias.add(mat1);
		listaMaterias.add(mat2);
		listaMaterias.add(mat3);
		listaMaterias.add(mat4);

		// generamos el alumno

		Alumno alu = new Alumno(466, "Juan", "Perez", new Date(), carr);

		contPers.crearAlumno(alu);

		System.out.println("Proceso finalizado");

//		
//
//		Carrera carr = new Carrera(45, "Ingenieria de Sistemas");
//
//		contPers.crearCarrera(carr);
//
//		Alumno alu = new Alumno(450, "Carlos", "Rocabado el BASTA", new Date(), carr);
//
//		contPers.crearAlumno(alu);
//
//		// materias
//		Materia mat1 = new Materia(12, "Programacion 1", "Semestral");
//		Materia mat2 = new Materia(13, "Estructura de datos", "Semestral");
//		Materia mat3 = new Materia(15, "Taller de Programacion", "Semestral");
//		Materia mat4 = new Materia(16, "Taller de Tesis", "Anual");
//
//		contPers.crearMateria(mat1);
//		contPers.crearMateria(mat2);
//		contPers.crearMateria(mat3);
//		contPers.crearMateria(mat4);
//
//		System.out.println("Los datos del alumno son: " + alu);
//
//		System.out.println("Y la carrera de " + alu.getNombre() + " es " + alu.getCarrera().getNombre());
//
//		System.out.println("-------------- LISTANDO TODOS LOS ALUMNOS -------------");
//
//		// Editar
//
////		Alumno alu2 = new Alumno(60, "Juan", "Morales", new Date());
////		
////		contPers.crearAlumno(alu2);
////		
////		// modificar
////		alu2.setNombre("MARCELO");
////		
////		contPers.actualizarAlumno(alu2);
//
//		// contPers.eliminarAlumno(12);
//
//		System.out.println("-------------- LISTANDO UN ALUMNO -------------");
//		Alumno aux = contPers.obtenerAlumno(50);
//		

//		
//		ArrayList<Alumno> vectorAlumnos = contPers.obtenerTodosAlumnos();
//		
//		for(Alumno al : vectorAlumnos){
//			System.out.println("Los datos del alumno son: " + al);
//		}
	}

}
