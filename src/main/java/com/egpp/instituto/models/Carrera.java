package com.egpp.instituto.models;

import java.util.LinkedList;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Carrera {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	@Basic
	private String nombre;

	
	@OneToMany(mappedBy="carrera")
	private LinkedList<Materia> materias;

	public Carrera() {
		// TODO Auto-generated constructor stub
	}

	public Carrera(int id, String nombre, LinkedList<Materia> materias) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.materias = materias;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Materia> getMaterias() {
		return materias;
	}

	public void setMaterias(LinkedList<Materia> materias) {
		this.materias = materias;
	}

}
